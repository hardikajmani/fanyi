#%%
import pandas as pd
import numpy as np
import sys
import string
import re
from pickle import dump
from unicodedata import normalize
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.utils.vis_utils import plot_model
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import RepeatVector
from keras.layers import TimeDistributed
from keras.callbacks import ModelCheckpoint


#%%
# load doc into memory
def load_doc(filename):
    #open the files in readmode only
    file = open(filename, mode='rt', encoding='utf-8')
    #read all text
    text = file.read()
    #close te file
    file.close()
    return text

#%%
def to_pairs(doc):
    lines = doc.strip().split('\n')
    pairs = [line.split('\t') for line in lines]
    return pairs

#%%
#clean list of pairs
def clean_pairs(lines):
    cleaned = list()
    #regex for filtering
    re_print = re.compile('[^%s]' % re.escape(string.printable))
    #prepare translation table for removing punctutation
    table = str.maketrans('','',string.punctuation)
    for pair in lines:
        clean_pair = list()
        for line in pair:
            #normalize the unicode characters
            line = normalize('NFD', line).encode('ascii', 'ignore')
            line = line.decode('utf-8')
            #tokenize whitespacee
            line = line.split()
            # convert to lowercase
            line = [word.lower() for word in line]
            # remove punctutation from each token
            line = [word.translate(table) for word in line]
            # remove non printable chars from each token
            line = [re_print.sub('', w) for w in line]
            #remove tokens with numbers in them
            line = [word for word in line if word.isalpha()]
            # store as string
            clean_pair.append(' '.join(line))
        cleaned.append(clean_pair)
    return np.array(cleaned)
#%%
#save a list of clean sentence to file
def save_clean_data(sentences, filename):
    dump(sentences, open(filename, 'wb'))
    print('Saved: %s' % filename)

#%%
# load dataset
filename = 'data//deu.txt'
doc = load_doc(filename)
#split into english german pairs
pairs = to_pairs(doc)
# clean sentences
clean_pairs = clean_pairs(pairs)
#save the cleaned data
save_clean_data(clean_pairs, 'english-german.pkl')
#spot check
for i in range(100):
    print('[%s] => [%s]' % (clean_pairs[i,0], clean_pairs[i,1]))

#%%
from pickle import load

#load a clean dataset
def load_clean_sentences(filename):
    return load(open(filename, 'rb'))

#load dataset  
raw_dataset = load_clean_sentences('english-german.pkl')

#reduce dataset size
n_sentences = 10000
dataset = raw_dataset[:n_sentences,:]

#random shuffle
np.random.shuffle(dataset)

#split into train/test
train, test = dataset[:9000], dataset[9000:]

#save the datase
save_clean_data(dataset, 'english-german-both.pkl')
save_clean_data(train, 'english-german-train.pkl')
save_clean_data(test, 'english-german-test.pkl')
print("done")

#%%
def create_tokenizer(lines):
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(lines)
    return tokenizer

#%%
def max_length(lines):
    return max(len(line.split()) for line in lines)

#%%
eng_tokenizer = create_tokenizer(dataset[:, 0])
eng_vocab_size = len(eng_tokenizer.word_index) + 1
eng_length = max_length(dataset[:, 0])
print('English Vocab size: %d' % eng_vocab_size)
print('English Max length: %d' % (eng_length))

#prepare german tokenizer
ger_tokenizer = create_tokenizer(dataset[:, 1])
ger_vocab_size = len(ger_tokenizer.word_index) + 1
ger_length = max_length(dataset[:, 1])
print('German Vocab size: %d' % ger_vocab_size)
print('German Max length: %d' % (ger_length))

#%%
#encode and pad sequences
def encode_sequences(tokenizer, length, lines):
    #integer encode sequences
    X = tokenizer.texts_to_sequences(lines)
    #pad sequences with 0 values
    X = pad_sequences(X, maxlen = length, padding = 'post')
    return X
#%%
# one hot encode target sequence
def encode_output(sequences, vocab_size):
    ylist = list()
    for sequence in sequences:
        encoded = to_categorical(sequence, num_classes=vocab_size)
        ylist.append(encoded)
    y = np.array(ylist)
    y = y.reshape(sequences.shape[0], sequences.shape[1], vocab_size)
    return y

#%%
#prepare the data
trainX = encode_sequences(ger_tokenizer, ger_length, train[:, 1])
trainY = encode_sequences(eng_tokenizer, eng_length, train[:, 0])
trainY = encode_output(trainY, eng_vocab_size)

#prepare the validation data
testX = encode_sequences(ger_tokenizer, ger_length, test[:, 1])
testY = encode_sequences(eng_tokenizer, eng_length, test[:, 0])
testY = encode_output(testY, eng_vocab_size)


#%% 
#definr the model
def define_model(src_vocab, tar_vocab, src_timesteps, tar_timesteps, n_units):
    model = Sequential()
    model.add(Embedding(src_vocab, n_units, input_length=src_timesteps, mask_zero = True))
    model.add(LSTM(n_units))
    model.add(RepeatVector(tar_timesteps))
    model.add(LSTM(n_units, return_sequences=True))
    model.add(TimeDistributed(Dense(tar_vocab, activation='softmax')))
    return model

#%%
# define model
model = define_model(ger_vocab_size, eng_vocab_size, ger_length, eng_length, 256)
model.compile(optimizer='adam', loss='categorical_crossentropy')

# summarize defined model
print(model.summary())
#plot_model(model, to_file='model.png', show_shapes=True)

#%%
#fir the model
filename = "model2.h5"
checkpoint = ModelCheckpoint(filename, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
model.fit(trainX, trainY, epochs = 50, batch_size=64, validation_data = (testX, testY), callbacks = [checkpoint], verbose = 2)
